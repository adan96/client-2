package com.eureka.client.service;

import com.eureka.client.model.entity.Movie;
import com.eureka.client.model.entity.Response;
import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@RibbonClient(name = "movie-service")
@Service
@DefaultProperties(
        threadPoolProperties = {
                @HystrixProperty( name = "coreSize", value = "10" ),
                @HystrixProperty( name = "maxQueueSize", value = "15"),
                @HystrixProperty( name = "queueSizeRejectionThreshold", value = "100")},
        commandProperties = {
                @HystrixProperty( name = "execution.isolation.thread.timeoutInMilliseconds", value = "5000"),
                @HystrixProperty( name = "circuitBreaker.requestVolumeThreshold", value = "10"),
                @HystrixProperty( name = "circuitBreaker.errorThresholdPercentage", value = "50"),
                @HystrixProperty( name = "circuitBreaker.sleepWindowInMilliseconds", value = "6000"),
                @HystrixProperty( name = "metrics.rollingStats.timeInMilliseconds", value = "6000"),
                @HystrixProperty( name = "metrics.rollingStats.numBuckets", value = "10") } )
public class RecommendationService {

    @Autowired
    private RestTemplate restTemplate;

    @HystrixCommand(fallbackMethod = "getRecommentationBackup", threadPoolKey = "getThreadPool")
    public ResponseEntity getRecommendations(){
        Movie[] vector = restTemplate.getForObject("http://movie-service/movies", Movie[].class);
        List<Movie> movies = Arrays.asList(vector);
        return new ResponseEntity(movies, HttpStatus.OK);
    }

    public ResponseEntity getRecommentationBackup(){
        return new ResponseEntity<>(
                "Esto puede tomar un tiempo", HttpStatus.REQUEST_TIMEOUT);
    }

}

package com.eureka.client.utils;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface Message {

    @Output("channelMessage")
    MessageChannel message();

    @Output("channelStandardMessage")
    MessageChannel standardMessage();

}

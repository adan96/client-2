package com.eureka.client.utils;

import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.Bean;
import org.springframework.integration.annotation.InboundChannelAdapter;
import org.springframework.integration.annotation.Poller;
import org.springframework.integration.core.MessageSource;
import org.springframework.integration.support.MessageBuilder;

@EnableBinding(Message.class)
public class MessageToRabbit {

    @Bean
    //@InboundChannelAdapter(channel="channelMessage", poller = @Poller(fixedDelay = "2000"))//Cada dos segundos al canal
    public MessageSource<ToolMessage> sendMessageCharge(){
        //return "{station:\"20\", customerid:\"100\", timestamp:\"2018-07-13T16:30:00\"}";
        return () -> MessageBuilder.withPayload(new ToolMessage("20", "100", "2019-07-14T11:00:00")).build();
    }

    class ToolMessage {
        public String stationId;
        public String customerId;
        public String timestamp;

        public ToolMessage(String stationId, String customerId, String timestamp) {
            this.stationId = stationId;
            this.customerId = customerId;
            this.timestamp = timestamp;
        }
    }

}
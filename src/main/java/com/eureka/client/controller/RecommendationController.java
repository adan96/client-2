package com.eureka.client.controller;

import com.eureka.client.service.RecommendationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class RecommendationController {

    @Autowired
    private RecommendationService recommendationService;

    @GetMapping(value = "/recommendations")
    public ResponseEntity getRecommendations(){
        return recommendationService.getRecommendations();
    }

}

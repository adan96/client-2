package com.eureka.client.controller;

import com.eureka.client.utils.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;

@RestController
public class OtherController {

    @Autowired
    Message message;

    @RequestMapping(path = "/payload", method = RequestMethod.POST)
    public String publishMessage(@RequestBody String payload){
        System.out.println(payload);
        Random r = new Random();
        message.message().send(MessageBuilder.withPayload(payload).setHeader("speed", r.nextInt(8)*10).build());
        return "success";
    }

}
